import Orders from './components/Orders';
import OrderDetails from './components/OrderDetails';

const routes = [
    {path: '/', component: Orders},
    {path: '/order-details/:id', component: OrderDetails}
]

export default routes